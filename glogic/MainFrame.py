# -*- coding: utf-8; indent-tabs-mode: t; tab-width: 4 -*-

import copy, os, sys, webbrowser
from gi.repository import Gtk, Gdk, GdkPixbuf
from glogic import config, const
from glogic.Exporter import save_schematics_as_image
from gettext import gettext as _
from glogic.DrawArea import DrawArea
from glogic.ComponentWindow import ComponentWindow
from glogic.CircuitManager import CircuitManager
from glogic.PropertyWindow import PropertyWindow
from glogic.PreferencesWindow import PreferencesWindow
from glogic import Preference
from glogic.Components import *
from glogic.TimingDiagramWindow import TimingDiagramWindow
from glogic.ComponentConverter import components_to_string, string_to_components

themed_icons = Gtk.IconTheme.get_default()
themed_icons.append_search_path(config.DATADIR+"/images")

class MainFrame(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="%s - %s" % (const.text_notitle, const.app_name))

		self.running_mode = False
		self.pause_running_mode = False
		self.clicked_on_pause = False

		self.circuit = CircuitManager()
		self.circuit.connect("title-changed", self.on_circuit_title_changed)
		self.circuit.connect("message-changed", self.on_circuit_message_changed)
		self.circuit.connect("item-unselected", self.on_circuit_item_unselected)
		self.circuit.connect("alert", self.on_circuit_alert)

		Preference.load_settings()

		self.create_window()

		# Component window
		self.comp_window = ComponentWindow()
		self.comp_window.connect("component-checked", self.on_comp_checked)
		self.comp_window.connect("window-hidden", self.on_compwindow_hidden)
		self.comp_window.set_transient_for(self)

		# Property window
		self.prop_window = PropertyWindow()
		self.prop_window.set_transient_for(self)
		self.prop_window.connect("window-hidden", self.on_propwindow_hidden)
		self.prop_window.connect("property-changed", self.on_property_changed)

		# Timing diagram window
		self.diagram_window = TimingDiagramWindow(self)

		# Preferences window
		self.pref_window = PreferencesWindow(self)

		# About dialog
		self.about_dialog = Gtk.AboutDialog()
		self.about_dialog.set_logo(GdkPixbuf.Pixbuf.new_from_file(config.DATADIR+"/images/glogic.png"))
		self.about_dialog.set_program_name(const.app_name)
		self.about_dialog.set_version(config.VERSION if config.BZRREV == "" else "%s (+bzr%s)" % (config.VERSION, config.BZRREV))
		self.about_dialog.set_comments(const.description)
		self.about_dialog.set_copyright(const.copyright)
		self.about_dialog.set_website(const.website)
		self.about_dialog.set_license(const.license)
		self.about_dialog.set_authors(const.developer)
		tr_credits = _("translator-credits")
		if tr_credits != "translator-credits":
			self.about_dialog.set_translator_credits(tr_credits)

		self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

		if len(sys.argv) >= 2:
			self.circuit.open_file(sys.argv[1])
			self.drawarea.redraw = True
			self.drawarea.queue_draw()

	def create_window(self):
		self.set_default_size(640, 400)
		box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

		menu_xml = """
		<ui>
			<menubar name="MenuBar">
				<menu action="file">
					<menuitem action="new"/>
					<menuitem action="open"/>
					<separator/>
					<menuitem action="save"/>
					<menuitem action="saveas"/>
					<separator/>
					<menuitem action="saveimage"/>
					<separator/>
					<menuitem action="quit"/>
				</menu>
				<menu action="edit">
					<menuitem action="undo"/>
					<menuitem action="redo"/>
					<separator/>
					<menuitem action="cut"/>
					<menuitem action="copy"/>
					<menuitem action="paste"/>
					<menuitem action="delete"/>
					<separator/>
					<menuitem action="property"/>
					<separator/>
					<menuitem action="rotleft"/>
					<menuitem action="rotright"/>
					<menuitem action="fliphori"/>
					<menuitem action="flipvert"/>
					<separator/>
					<menuitem action="prefs"/>
				</menu>
				<menu action="add">
					<menuitem action="components"/>
					<menuitem action="net"/>
				</menu>
				<menu action="simulate">
					<menuitem action="run"/>
					<menuitem action="pause-run"/>
					<menuitem action="diagram"/>
				</menu>
				<menu action="help">
					<menuitem action="contents"/>
					<separator/>
					<menuitem action="trans"/>
					<menuitem action="report"/>
					<separator/>
					<menuitem action="about"/>
				</menu>
			</menubar>
			<toolbar name="ToolBar">
				<toolitem action="new" />
				<toolitem action="open" />
				<toolitem action="save" />
				<separator/>
				<toolitem action="undo" />
				<toolitem action="redo" />
				<separator/>
				<toolitem action="components" />
				<toolitem action="net" />
				<separator/>
				<toolitem action="run" />
				<toolitem action="pause-run" />
			</toolbar>
		</ui>
		"""

		actiongroup = Gtk.ActionGroup("menu")
		actions = [
			("new",       Gtk.STOCK_NEW, _("_New"), "<Control>N", _("Close this circuit and create a new one."), self.on_action_new_pressed),
			("open",      Gtk.STOCK_OPEN, _("_Open..."), "<Control>O", _("Close this circuit and open the other one."), self.on_action_open_pressed),
			("save",      Gtk.STOCK_SAVE, _("_Save"), "<Control>S", _("Save this circuit."), self.on_action_save_pressed),
			("saveas",    Gtk.STOCK_SAVE_AS, _("Save _As..."), "<Shift><Control>S", _("Save this circuit with a new name."), self.on_action_saveas_pressed),
			("saveimage", None, _("Save as _image..."), None, _("Save schematics as image file."), self.on_action_save_image),
			("quit",      Gtk.STOCK_QUIT, _("_Quit"), "<Control>Q", _("Close this application."), self.on_action_quit_pressed),
			("undo",      Gtk.STOCK_UNDO, _("_Undo"), "<Control>Z", _("Undo the previous action."), self.on_action_undo_pressed),
			("redo",      Gtk.STOCK_REDO, _("_Redo"), "<Shift><Control>Z", _("Redo the action that you have canceled."), self.on_action_redo_pressed),
			("cut",       Gtk.STOCK_CUT, _("Cu_t"), "<Control>X", _("Cut selected components."), self.on_action_cut_pressed),
			("copy",      Gtk.STOCK_COPY, _("_Copy"), "<Control>C", _("Copy selected components."), self.on_action_copy_pressed),
			("paste",     Gtk.STOCK_PASTE, _("_Paste"), "<Control>V", _("Paste copied components."), self.on_action_paste_pressed),
			("delete",    Gtk.STOCK_DELETE, _("_Delete"), "Delete", _("Delete selected components."), self.on_action_delete_pressed),
			("rotleft",   None, _("Rotate _left 90"), "L", _("Rotate selected components 90 degrees."), self.on_action_rotate_left_90),
			("rotright",  None, _("Rotate _right 90"), "R", _("Rotate selected components -90 degrees."), self.on_action_rotate_right_90),
			("fliphori",  None, _("Flip _horizontally"), "H", _("Flip components horizontally."), self.on_action_flip_horizontally),
			("flipvert",  None, _("Flip _vertically"), "V", _("Flip components vertically."), self.on_action_flip_vertically),
			("prefs",     Gtk.STOCK_PREFERENCES, _("Pr_eferences"), None, _("Set preferences of this application."), self.on_action_prefs_pressed),
			("contents",  Gtk.STOCK_HELP, _("_Contents"), None, _("Show the help browser."), self.on_action_show_help),
			("trans",     None, _("Translate This Application..."), None, _("Connect to the Launchpad website to help translate this application."), self.on_action_translate_pressed),
			("report",    None, _("Report a Problem..."), None, _("Connect to the Launchpad website to report a problem of this application."), self.on_action_bug_pressed),
			("about",     Gtk.STOCK_ABOUT, _("_About"), None, _("Show about dialog."), self.on_action_about_pressed),
			("file",      None, _("_File")),
			("edit",      None, _("_Edit")),
			("add",       None, _("_Add")),
			("simulate",  None, _("_Simulate")),
			("help",      None, _("_Help"))
		]
		toggle_actions = [
			("property",   Gtk.STOCK_PROPERTIES, _("_Properties"), "<Control>P", _("Show property dialog."), self.on_action_property_toggled),
			("components", None, _("_Components..."), "<Control>A", _("Show components window."), self.on_btn_add_components_toggled),
			("net",        None, _("_Net"), "<Control>E", _("Add nets to this circuit."), self.on_action_net_toggled),
			("run",        Gtk.STOCK_MEDIA_PLAY, _("_Run"), "F5", _("Run and simulate this circuit."), self.on_action_run_toggled),
			("pause-run",  Gtk.STOCK_MEDIA_PAUSE, _("_Pause"), "F6", _("Pause simulation to keep the state when components are clicked."), self.on_action_pause_run_toggled),
			("diagram",    None, _("_Timing Diagram"), "<Control>T", _("Show timing diagram window."), self.on_action_diagram_pressed),
		]

		actiongroup.add_actions(actions)
		actiongroup.add_toggle_actions(toggle_actions)

		self.action_undo = actiongroup.get_action("undo")
		self.action_redo = actiongroup.get_action("redo")
		self.action_cut = actiongroup.get_action("cut")
		self.action_copy = actiongroup.get_action("copy")
		self.action_paste = actiongroup.get_action("paste")
		self.action_delete = actiongroup.get_action("delete")
		self.action_property = actiongroup.get_action("property")
		self.action_rotleft = actiongroup.get_action("rotleft")
		self.action_rotright = actiongroup.get_action("rotright")
		self.action_fliphori = actiongroup.get_action("fliphori")
		self.action_flipvert = actiongroup.get_action("flipvert")
		self.action_components = actiongroup.get_action("components")
		self.action_net = actiongroup.get_action("net")
		self.action_run = actiongroup.get_action("run")
		self.action_pause_run = actiongroup.get_action("pause-run")
		self.action_diagram = actiongroup.get_action("diagram")

		self.action_undo.set_sensitive(False)
		self.action_redo.set_sensitive(False)
		self.action_diagram.set_sensitive(False)
		self.action_pause_run.set_sensitive(False)

		uimanager = Gtk.UIManager()
		uimanager.add_ui_from_string(menu_xml)
		self.add_accel_group(uimanager.get_accel_group())
		uimanager.insert_action_group(actiongroup)

		action = actiongroup.get_action("components")
		action.set_icon_name("add-component")
		action = actiongroup.get_action("net")
		action.set_icon_name("add-net")

		# Menu bar
		menubar = uimanager.get_widget("/MenuBar")
		box.pack_start(menubar, False, False, 0)

		# Tool bar
		toolbar = uimanager.get_widget("/ToolBar")
		toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)
		box.pack_start(toolbar, False, False, 0)

		# Draw area
		self.drawarea = DrawArea(self)
		self.drawarea.circuit = self.circuit
		box.pack_start(self.drawarea, False, True, 0)

		# Status bar
		self.statusbar = Gtk.Statusbar()
		box.pack_start(self.statusbar, False, False, 0)

		self.add(box)

		# Connect events
		self.connect("delete-event", self.on_window_delete)

		self.disable_edit_actions()

	def on_action_about_pressed(self, widget):
		self.about_dialog.run()
		self.about_dialog.hide()

	def on_action_new_pressed(self, widget):
		if self.check_modified():
			return
		if self.drawarea.drag_enabled:
			return

		self.set_title("%s - %s" % (const.text_notitle, const.app_name))
		self.reset_frame()
		self.circuit.reset_circuit()
		self.drawarea.nearest_component = None
		self.drawarea.redraw = True
		self.drawarea.queue_draw()

	def add_filters(self, dialog):
		filter_wxl = Gtk.FileFilter()
		filter_wxl.set_name(const.glcfile_text)
		filter_wxl.add_pattern("*.glc")
		dialog.add_filter(filter_wxl)
		filter_any = Gtk.FileFilter()
		filter_any.set_name(const.anyfile_text)
		filter_any.add_pattern("*")
		dialog.add_filter(filter_any)

	def reset_frame(self):
		self.comp_window.uncheck_all_buttons()
		# reset prop window
		self.statusbar.push(0, "")
		self.comp_window.checked_button = const.component_none
		self.drawarea.set_component(const.component_none)
		self.action_undo.set_sensitive(False)
		self.action_redo.set_sensitive(False)
		self.disable_edit_actions()
		self.action_diagram.set_sensitive(False)
		self.action_net.set_active(False)
		self.action_run.set_active(False)
		self.action_diagram.set_active(False)
		self.diagram_window.destroy()
		self.diagram_window = TimingDiagramWindow(self)

	def on_action_open_pressed(self, widget):
		if self.check_modified():
			return
		while True:
			dialog = Gtk.FileChooserDialog(_("Open file"), self, Gtk.FileChooserAction.OPEN, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.ACCEPT))
			dialog.set_transient_for(self)
			self.add_filters(dialog)
			if dialog.run() == Gtk.ResponseType.ACCEPT:
				filepath = dialog.get_filename()
			else:
				break

			if not self.circuit.open_file(filepath):
				self.reset_frame()
				self.drawarea.redraw = True
				self.drawarea.queue_draw()
				break

			dialog.destroy()

		dialog.destroy()

	def overwrite_save(self):
		if self.circuit.filepath == "":
			return self.rename_save()
		else:
			if self.circuit.save_file(self.circuit.filepath):
				return self.rename_save()
			return False

	def rename_save(self):
		chooser = Gtk.FileChooserDialog(_("Save file"), self, Gtk.FileChooserAction.SAVE, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT), flags=Gtk.DialogFlags.MODAL)
		chooser.set_transient_for(self)
		chooser.set_modal(True) 
		self.add_filters(chooser)
		while True:
			if chooser.run() == Gtk.ResponseType.ACCEPT:
				filepath = chooser.get_filename()
				filter_name = chooser.get_filter().get_name()
				if filter_name == const.glcfile_text:
					if not "." in os.path.basename(filepath):
						filepath += ".glc"

				if os.path.exists(filepath):
					dialog = Gtk.MessageDialog(chooser, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, _("Overwrite to the existing file?"))
					dialog.format_secondary_text(_("The file already exist. Overwrite it?"))
					retval = dialog.run()
					dialog.destroy()
					if retval == Gtk.ResponseType.NO:
						continue

			else:
				chooser.destroy()
				return True

			if not self.circuit.save_file(filepath):
				chooser.destroy()
				return False

	def on_action_save_pressed(self, widget):
		self.overwrite_save()

	def on_action_saveas_pressed(self, widget):
		self.rename_save()

	def on_action_quit_pressed(self, widget):
		event = Gdk.Event(Gdk.EventType.DELETE)
		self.emit("delete-event", event)

	def check_modified(self):
		if self.circuit.need_save:
			dialog = Gtk.MessageDialog(self, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, _("Save the modified schematics?"))
			dialog.format_secondary_text(_("The schematics was modifed. Save the changes before closing?"))
			retval = dialog.run()
			dialog.destroy()
			if retval == Gtk.ResponseType.YES:
				return self.overwrite_save()
			elif retval == Gtk.ResponseType.NO:
				return False
			else:
				return True
		return False

	def on_btn_add_components_toggled(self, widget):
		if widget.get_active():
			self.comp_window.show_all()
		else:
			self.comp_window.hide()

	def on_window_delete(self, widget, event):
		if self.check_modified():
			return True
		Gtk.main_quit()
		return False

	def on_action_net_toggled(self, widget):
		if widget.get_active():
			self.drawarea.netstarted = False
			self.comp_window.uncheck_all_buttons()
			widget.set_active(True)
			self.drawarea.set_component(const.component_net)
		elif self.drawarea.get_component() == const.component_net:
			self.drawarea.set_component(const.component_none)
		self.drawarea.queue_draw()

	def on_action_run_toggled(self, widget):
		if self.drawarea.drag_enabled:
			return
		if self.running_mode:
			self.running_mode = False
			self.clicked_on_pause = False
			if self.circuit.action_count > 0:
				self.action_undo.set_sensitive(True)
			if self.circuit.action_count < len(self.circuit.components_history) - 1:
				self.action_redo.set_sensitive(True)
			self.action_property.set_sensitive(True)
			self.action_components.set_sensitive(True)
			self.action_net.set_sensitive(True)
			self.action_pause_run.set_sensitive(False)
			self.action_pause_run.set_active(False)
			self.action_diagram.set_sensitive(False)
			self.action_diagram.set_active(False)
			self.diagram_window.hide()
			self.statusbar.push(0, "")
		else:
			self.running_mode = True
			self.circuit.selected_components = []
			self.action_undo.set_sensitive(False)
			self.action_redo.set_sensitive(False)
			self.disable_edit_actions()
			self.action_property.set_sensitive(False)
			self.action_property.set_active(False)
			self.action_components.set_sensitive(False)
			self.action_components.set_active(False)
			self.action_net.set_sensitive(False)
			self.action_net.set_active(False)
			self.action_pause_run.set_sensitive(True)
			self.action_diagram.set_sensitive(True)
			self.comp_window.hide()
			self.prop_window.hide()
			self.comp_window.checked_button = const.component_none
			self.comp_window.uncheck_all_buttons()
			self.drawarea.set_component(const.component_none)
			self.drawarea.component_dragged = False
			self.drawarea.drag_enabled = False
			self.drawarea.rect_select_enabled = False
			self.circuit.analyze_connections()
			self.circuit.initialize_logic()
			if not self.circuit.analyze_logic():
				self.diagram_window.diagramarea.createDiagram()
		self.drawarea.redraw = True
		self.drawarea.queue_draw()

	def on_action_pause_run_toggled(self, widget):
		if self.pause_running_mode:
			self.pause_running_mode = False
			if self.clicked_on_pause:
				if not self.circuit.analyze_logic():
					self.diagram_window.diagramarea.createDiagram()
				self.drawarea.queue_draw()
				self.clicked_on_pause = False
		else:
			self.pause_running_mode = True

	def on_action_cut_pressed(self, widget):
		self.on_action_copy_pressed(widget)
		self.on_action_delete_pressed(widget)

	def on_action_copy_pressed(self, widget):
		self.clipboard.set_text(components_to_string(self.circuit.selected_components), -1)
		self.clipboard.store()

	def on_action_paste_pressed(self, widget):
		str_data = self.clipboard.wait_for_text()
		if str_data != None:
			tmp = string_to_components(str_data)
			if isinstance(tmp, str):
				dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _("Error"))
				dialog.format_secondary_text(tmp)
				dialog.run()
				dialog.destroy()
				return
			else:
				pasted_components = tmp

			if not pasted_components:
				return

			self.comp_window.uncheck_all_buttons()
			self.drawarea.set_component(const.component_none)
			self.drawarea.set_pasted_components(pasted_components)

	def on_action_undo_pressed(self, widget):
		self.circuit.undo()
		if self.circuit.action_count == 0:
			self.action_undo.set_sensitive(False)
		self.action_redo.set_sensitive(True)
		self.disable_edit_actions()
		self.drawarea.redraw = True
		self.drawarea.queue_draw()

	def on_action_redo_pressed(self, widget):
		self.circuit.redo()
		self.action_undo.set_sensitive(True)
		if self.circuit.action_count == len(self.circuit.components_history) - 1:
			self.action_redo.set_sensitive(False)
		self.disable_edit_actions()
		self.drawarea.redraw = True
		self.drawarea.queue_draw()

	def on_action_delete_pressed(self, widget):
		self.circuit.remove_selected_component()
		self.drawarea.nearest_component = None
		self.drawarea.preselected_component = None
		self.circuit.push_history()
		self.action_undo.set_sensitive(True)
		self.action_redo.set_sensitive(False)
		self.disable_edit_actions()
		self.drawarea.redraw = True
		self.drawarea.queue_draw()

	def on_action_rotate_left_90(self, widget):
		if comp_dict[self.drawarea.get_component()] is None:
			self.circuit.rotate_left_selected_components()
			self.circuit.push_history()
			self.action_undo.set_sensitive(True)
			self.action_redo.set_sensitive(False)
			self.drawarea.redraw = True
		else:
			self.drawarea.rotate_left_picked_components()
		self.drawarea.queue_draw()

	def on_action_rotate_right_90(self, widget):
		if comp_dict[self.drawarea.get_component()] is None:
			self.circuit.rotate_right_selected_components()
			self.circuit.push_history()
			self.action_undo.set_sensitive(True)
			self.action_redo.set_sensitive(False)
			self.drawarea.redraw = True
		else:
			self.drawarea.rotate_right_picked_components()
		self.drawarea.queue_draw()

	def on_action_flip_horizontally(self, widget):
		if comp_dict[self.drawarea.get_component()] is None:
			self.circuit.flip_hori_selected_components()
			self.circuit.push_history()
			self.action_undo.set_sensitive(True)
			self.action_redo.set_sensitive(False)
			self.drawarea.redraw = True
		else:
			self.drawarea.flip_hori_picked_components()
		self.drawarea.queue_draw()

	def on_action_flip_vertically(self, widget):
		if comp_dict[self.drawarea.get_component()] is None:
			self.circuit.flip_vert_selected_components()
			self.circuit.push_history()
			self.action_undo.set_sensitive(True)
			self.action_redo.set_sensitive(False)
			self.drawarea.redraw = True
		else:
			self.drawarea.flip_vert_picked_components()
		self.drawarea.queue_draw()

	def on_action_property_toggled(self, widget):
		if widget.get_active():
			self.drawarea.set_selected_component_to_prop_window()
			self.prop_window.show_all()
		else:
			self.prop_window.hide()

	def on_action_show_help(self, widget):
		Gtk.show_uri(None, const.help, Gdk.CURRENT_TIME)

	def on_action_translate_pressed(self, widget):
		webbrowser.open(const.devel_translate)

	def on_action_bug_pressed(self, widget):
		webbrowser.open(const.devel_bug)

	def on_action_diagram_pressed(self, widget):
		if widget.get_active():
			self.diagram_window.show_all()
		else:
			self.diagram_window.hide()

	def on_action_save_image(self, widget):
		save_schematics_as_image(self.circuit, self.running_mode, self)

	def on_action_prefs_pressed(self, widget):
		self.pref_window.update_dialog()
		if self.pref_window.run() == Gtk.ResponseType.APPLY:
			self.pref_window.apply_settings()
			Preference.save_settings()
			self.drawarea.redraw = True
			self.drawarea.queue_draw()
		self.pref_window.hide()

	def on_comp_checked(self, widget, comp_name):
		if comp_dict[comp_name]:
			self.action_rotleft.set_sensitive(True)
			self.action_rotright.set_sensitive(True)
			self.action_fliphori.set_sensitive(True)
			self.action_flipvert.set_sensitive(True)
		elif not self.circuit.selected_components:
			self.action_rotleft.set_sensitive(False)
			self.action_rotright.set_sensitive(False)
			self.action_fliphori.set_sensitive(False)
			self.action_flipvert.set_sensitive(False)
		if comp_name != const.component_none:
			self.action_net.set_active(False)
		elif self.drawarea.get_component() == const.component_net:
			return
		self.drawarea.set_component(comp_name)
		self.drawarea.queue_draw()

	def on_compwindow_hidden(self, widget):
		self.action_components.set_active(False)

	def on_propwindow_hidden(self, widget):
		self.action_property.set_active(False)

	def on_property_changed(self, widget):
		self.drawarea.redraw = True
		self.drawarea.queue_draw()
		self.circuit.push_history()
		self.action_undo.set_sensitive(True)

	def on_circuit_title_changed(self, circuit, title):
		self.set_title(title)

	def on_circuit_message_changed(self, circuit, message):
		self.statusbar.push(0, message)

	def on_circuit_item_unselected(self, circuit):
		self.prop_window.setComponent(None)

	def on_circuit_alert(self, circuit, message):
		dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, _("Error"))
		dialog.format_secondary_text(message)
		dialog.run()
		dialog.destroy()

	def disable_edit_actions(self):
		self.action_cut.set_sensitive(False)
		self.action_copy.set_sensitive(False)
		self.action_delete.set_sensitive(False)
		if comp_dict[self.drawarea.get_component()] is None:
			self.action_rotleft.set_sensitive(False)
			self.action_rotright.set_sensitive(False)
			self.action_fliphori.set_sensitive(False)
			self.action_flipvert.set_sensitive(False)
