<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="edit" xml:lang="ja">

	<info>
		<link type="guide" xref="index#edit"/>
		<desc>部品選択モードでは、部品の移動や削除ができます。</desc>
		<revision pkgversion="2.6" version="0.1" date="2012-10-08" status="final"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="credit.xml"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
	</info>

	<title>部品の移動と削除</title>

	<section id="add"><title>部品の選択</title>
		<p>gLogic を起動すると、まず部品選択モードになります。配線追加モードまたは部品追加モードから部品選択モードに変更するには、数回右クリックします。部品を選択する方法は2つあります:</p>
		<list>
			<item>
				<p>カーソルを部品の上に移動し、部品の色が変わったらクリックする</p>
			</item>
			<item>
				<p>画面上でカーソルをドラッグさせ、短形範囲の部品すべてを選択する</p>
			</item>
		</list>
		<p>選択部品を追加するには、<key>Ctrl</key> を押しながら部品を選択します。部品の選択を一つ解除したい場合は、<key>Ctrl</key> を押しながら部品をクリックします。</p>
		<p>すべての部品の選択を解除するには、部品以外の場所をクリックします。</p>
	</section>

	<section id="move"><title>部品の移動</title>
		<p>部品を移動するには、選択された部品の上にカーソルを置いてドラッグします。</p>
	</section>

	<section id="rotate"><title>部品の回転と反転</title>
		<p>以下の4つの部品変換方法があります。</p>
		<table frame="all" rules="all">
			<tr>
				<td><p>メニュー</p></td>
				<td><p>キー</p></td>
				<td><p>説明</p></td>
			</tr>
			<tr>
				<td><p>90度左回転</p></td>
				<td its:translate="no"><p><key>L</key></p></td>
				<td><p>部品を反時計回りに90度回転します。</p></td>
			</tr>
			<tr>
				<td><p>90度右回転</p></td>
				<td its:translate="no"><p><key>R</key></p></td>
				<td><p>部品を時計回りに90度回転します。</p></td>
			</tr>
			<tr>
				<td><p>水平方向の反転</p></td>
				<td its:translate="no"><p><key>H</key></p></td>
				<td><p>部品を左右反転します。</p></td>
			</tr>
			<tr>
				<td><p>垂直方向の反転</p></td>
				<td its:translate="no"><p><key>V</key></p></td>
				<td><p>部品を上下反転します。</p></td>
			</tr>
		</table>
	</section>

	<section id="delete"><title>部品の削除</title>
		<p>部品を選択して <key>Delete</key> を押すか、 <guiseq><gui>編集</gui><gui>削除</gui></guiseq> をクリックすることで部品を削除できます。</p>
	</section>

</page>
